FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:el9 AS builder

ARG RELEASE=tdaq-99-00-00

RUN mkdir -p /work/build
RUN cp /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake/templates/CMakeLists.txt /work/CMakeLists.txt
RUN git clone --depth 1 https://gitlab.cern.ch/atlas-tdaq-software/IPCGatewayProxy.git /work/IPCGatewayProxy
RUN cd /work/build && source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh $RELEASE && cmake .. && make -j $(nproc) install

FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:el9

ARG RELEASE=tdaq-99-00-00

ENV GW_PUBLIC_PORT=8900
ENV GW_PUBLIC_HOST=test-proxy-gw.cern.ch
ENV GW_SSH_KEY=/work/keys/id_gwproxy
ENV GW_USER=gwproxy

# copy patches over
COPY --from=builder /work/installed /work/installed

EXPOSE 12345/tcp

COPY start_gateway /start_gateway
CMD /start_gateway
