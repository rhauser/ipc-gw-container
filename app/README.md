# Example application using IPC gateway proxy

This example deploys the `webis_server` to OKD
using the shared `ipc-gateway` service.

## Deployment

    ./deploy.sh <project-name> <hostname> <release> [ <release>...]

Note that it assumes that the gateway service is
provided by the `ipc-gateway` project.
