#!/bin/bash

usage()
{
  cat <<EOF
usage: $0 <project-name> <hostname> [ release....]
EOF
}

[ $# -ge 2 ]  || { usage >&2; exit 1; }

project=${1}
hostname=${2}

echo ${hostname} | grep -q \\. || hostname=${hostname}.app.cern.ch

shift 2

oc new-project ${project} --display-name="ATLAS Trigger/DAQ WebIS " --description="TDAQ WebIS for test bed" || true
oc create -f $(dirname $0)/network.yaml -f $(dirname $0)/../cvmfs.yaml

set -e 

for release in $@
do
  oc new-app --name=${release} --image gitlab-registry.cern.ch/rhauser/ipc-gw-container/server:${release} --env TDAQ_VERSION=${release} --labels role=frontend
  oc rollout pause deployment/${release}
  oc set volumes deployment/${release} --add -t pvc --claim-name cvmfs-volume         -m /cvmfs   --read-only
  oc rollout resume deployment/${release}
done

oc new-app --name=webis-nginx --context-dir nginx centos/nginx-116-centos7~https://gitlab.cern.ch/rhauser/ipc-gw-container

helm repo list | grep -q '^paas' || helm repo add paas https://gitlab.cern.ch/api/v4/projects/124069/packages/helm/master
helm repo update

helm install cern-proxy paas/cern-auth-proxy --set upstream.service.name=webis-nginx --set upstream.service.port=8080 --set route.hostname=${hostname}
