# IPCGatewayProxy for OKD/Openshift

This is a deployment of the ATLAS TDAQ IPCGatewayProxy
to  the CERN paas.cern.ch OKD4 cluster.

It allows to run TDAQ based web services inside the 
cluster with the full functionality of IPC, including
callbacks etc. This is typically not possible due to
the fact that applications on paas.cern.ch are hidden
behind a load balancer that only supports HTTP(S).

An example application can be seen in the [app](app) 
subdirectory of this package. It deploys the `webis_server`
for multiple parallel releases to paas.cern.ch and can
be reached [here](https://atlas-tdaq-webis.app.cern.ch).

## Using the IPCGatewayProxy

If you want to deploy a web application making use of TDAQ
in OKD, do the following:

  * Provide the name of your project to [me](mailto:reiner.hauser.cern.ch)
    (or make a merge request by making the change in [network.yaml](network.yaml) yourself).
  * Add [app/network.yaml](app/network.yaml) to your project.
    Modify/remove the label selectors if you have not labelled your
    own pods with `role=frontend`.
  * Make sure CVMFS is available (see [examples](cvmfs.yaml))
  * After setting up the TDAQ release in the pod, change the reference
    to the initial partition: 

    export TDAQ_IPC_INIT_REF=$(ipc_mk_ref -H ${CMTRELEASE}.ipc-gateway -P 12345)

You should then be able to see the testbed partitions for your release:

    ipc_ls -P
    is_ls -n PMG

You don't need anything from down here...

## Deploying IPCGatewayProxy as a service

To recreate the `ipc-gateway` project from scratch, run:

    ./deploy.sh ipc-gateway <path/to/ssh/key> <release> [ <release> ]

The private SSH key must allow access to bastion host.

Example:

    ./deploy.sh ipc-gateway ~/.ssh/id_gwproxy.ssh tdaq-99-00-00 tdaq-09-04-00

The resulting service is then available in `paas.cern.ch` for projects
specified in [network.yaml](network.yaml).

A release can be added later with:

    ./add-release tdaq-09-05-00

### Run with your own bastion host

The following is mostly for documentation. There should
be no need to have more than one such machine.

The setup uses a fixed external gateway host. It can
be used by several IPC proxies, but each must use a 
different port number.

Access to the host is via ssh and a special key:

```shell
ssh-keygen -t ed25519 -f ~/.ssh/id_gwproxy
```

On the host create a user `gwproxy` and add
`id_gwproxy.pub` to its `~/.ssh/authorized_keys` file.
Get all the permissions right...

Add this line to `/etc/ssh/sshd_config`:

```shell
Match User <username>
   GatewayPorts clientspecified
```

and `sudo systemctl restart sshd`.

The `username` should be the `username` used in 
`start_gateway` as login name.

If you run a firewall you also have to open all
relevant ports:

```shell
firewall-cmd --add-port 8900-8910/tcp
firewall-cmd --add-port 8900-8010/tcp --permanent
```

## Running it with podman or docker

```shell
podman pull gitlab-registry.cern.ch/rhauser/ipc-gw-container/gateway:nightly
podman run --rm \
   -p 8080:8080 \
   -v /cvmfs:/cvmfs:shared \
   -v /path/to/id_gwproxy:/work/keys/id_gwproxy \
   gitlab-registry.cern.ch/rhauser/ipc-gw-container/gateway:nightly
```
