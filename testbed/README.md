# IPCGatewayProxy with nodePort 

To start the gateway proxy:

    kubectl create -f service.yaml -f gateway.yaml 

See [example.yaml](example.yaml) and [partition.yaml](partition.yaml) for examples.

The first starts an IS server as part of the initial partition.
The second an `ipc_server` inside the cluster together with
an IS server.
