#!/bin/bash

# Use this to deploy from scratch into an emptry project.
# Use 'add-release' to add another release to the existing project

usage()
{
  cat <<EOF
echo "usage: $0 <project-name> <ssh-key-path> [ release....]"
EOF
}

[ $# -ge 2 ]  || { usage >&2 ; exit 1; }

project=${1}
key=${2}

shift 2

set -e 

oc new-project ${project} --display-name="ATLAS Trigger/DAQ IPC Gateway" --description="IPC gateway for testbed"

oc create secret generic ssh-key --from-file=${key}
oc create -f network.yaml -f cvmfs.yaml

for release in $@
do
  $(dirname $0)/add-release $release
  oc wait --for=condition=Available deploy/${release} --timeout=30s
done

